/**
 * Flashlight
 * @version 1.0
 * @author Grzegorz Banaś <grzegorz@uniti.pl>
 *
 * Hardware: NanJG 105d
 * Chip: ATTiny13A
 * Fuses: low( 0x75 ), high( 0xff )
 */

#define F_CPU 4800000

#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

// Modes
uint8_t Modes[] = { 5, 50, 255 };

// Define offset of clicks that is interpreted as double clicks
#define DOUBLE_CLICK_TIMEOUT 4

// Battery level when need to power off
#define BATTERY_MIN_VOLTAGE 120

// EEProm mode
unsigned int LAST_MODE EEMEM;

// Power off detection
volatile uint8_t powerOff __attribute__ ( ( section ( ".noinit" ) ) );

// Power off detection (double click)
volatile uint8_t powerOff_double __attribute__ ( ( section ( ".noinit" ) ) );

ISR( WDT_vect )
{
    if ( powerOff_double <= DOUBLE_CLICK_TIMEOUT )
        ++powerOff_double;
}

int main ( void )
{
    int lastMode, nextMode;

    DDRB = 2;
    TCCR0A = 0b00100001;
    TCCR0B = 0b00000001;

    EEARL = 4;
    EECR = 1;

    WatchDogInit();

    lastMode = (int)eeprom_read_word( &LAST_MODE );
    if ( lastMode < 0 || lastMode >= sizeof( Modes ) ) lastMode = 0;

    if ( powerOff )
    {
        // Wake up from long power off

        Change( 0, Modes[ lastMode ] );
    }
    else
    {
        if ( powerOff_double >= DOUBLE_CLICK_TIMEOUT )
        {
            // One click

            nextMode = ( lastMode + 1 );
            if ( nextMode < 0 || nextMode >= sizeof( Modes ) ) nextMode = 0;

            powerOff_double = 0;

            OCR0B = Modes[ lastMode ];

            _delay_ms( 500 );

            eeprom_busy_wait();
            eeprom_write_word( &LAST_MODE, nextMode );

            Change( Modes[ lastMode ], Modes[ nextMode ] );
        }
        else
        {
            // Double clicks

            if ( ADCSRA & ( 1 << ADIF ) )
            {
                for ( int i = 0; i < ADCH; i++ )
                {
                    OCR0B = 0;
                    _delay_ms( 500 );
                    OCR0B = 255;
                    _delay_ms( 500 );
                }
            }

            ADCSRA |= ( 1 << ADSC );

            // Change( 0, Modes[ lastMode ] );
            OCR0B = Modes[ lastMode ];
        }
    }

    powerOff = 0;
}

/**
 * Watchdog initialize function
 */
inline void WatchDogInit()
{
    cli();
    wdt_reset();
    WDTCR |= ( 1 << WDCE ) | ( 1 << WDE );
    WDTCR = ( 1 << WDTIE ) | ( 1 << WDP1 );
	sei();
}

/**
 * Watchdog destroy function
 */
inline void WatchDogDestroy()
{
    cli();
	wdt_reset();
	MCUSR &= ~( 1 << WDRF );
	WDTCR |= ( 1 << WDCE ) | ( 1 << WDE );
	WDTCR = 0x00;
	sei();
}

/**
 * Battery measure initialize
 */
inline void BatteryMeasureInitialize()
{
    ADMUX  = ( 1 << REFS0 ) | ( 1 << ADLAR ) | ADC_CHANNEL;
    DIDR0 |= ( 1 << ADC_DIDR );
    ADCSRA = ( 1 << ADEN ) | ( 1 << ADSC ) | ADC_PRSCL;
}

/**
 * Battery measure destroy
 */
inline void BatteryMeasureDestroy()
{
    ADCSRA &= ~( 1 << 7 );
}

/**
 * Smoothly change PWM
 */
int Change( int from, int to )
{
    do
    {
        if ( from > to ) from--;
        else from++;

        OCR0B = from;

        _delay_ms( 5 );
    }
    while ( from != to );
}